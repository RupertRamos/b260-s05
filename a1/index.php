<!DOCTYPE html>

<html>

    <head>

        <title>S05: Client-Server Communication (Activity)</title>

    </head>

    <body>

        <?php session_start(); ?>

        <?php if (!isset($_SESSION['email'])): ?>

            <form method="POST" action="./server.php">

                <input type="hidden" name="action" value="login"/>

                Email: <input type="text" name="email" required/>
                Password: <input type="password" name="password" required/>

                <button type="submit">Login</button>

                <?php if (isset($_SESSION['login_error_message'])): ?>

                    <p><?php echo $_SESSION['login_error_message']; ?></p>

                    <?php unset($_SESSION['login_error_message']) ?>

                <?php endif; ?>

            </form>

        <?php else: ?>

            <p>Hello, <?php echo $_SESSION['email']; ?></p>

            <form method="POST" action="./server.php">

                <input type="hidden" name="action" value="logout"/>
                <button type="submit">Logout</button>

            </form>

        <?php endif; ?>

    </body>

</html>