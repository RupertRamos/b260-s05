<?php 

    // $_GET and $_POST are super global variables in PHP.
    // These let you retrieve infromation sent by the client.
    // allows data to persist betweet pages / a single session.


    // The main difference between $_GET and $_POST is that $_GET appends the URL with "?param=value".
    // If you are to send sensitive data to the server, $_GET is not recommended.
    // You can use $_POST instead and just use $_GET for non-sensitive data. (e.g. "?sort=ascending").

    $tasks = ['Get git', 'Bake HTML', 'Eat Javascript', 'Learn PHP'];

    if(isset($_GET['index'])) {
        $indexGet = $_GET['index'];
        echo "The retrieved task from GET is $tasks[$indexGet]";
    }

    if(isset($_POST['index'])) {
        $indexPost = $_POST['index'];
        echo "The retrieved task from POST is $tasks[$indexPost]";
    }


?>



<!DOCTYPE html>

<html>

    <head>

        <title>S05: Client-Server Communication (GET and POST)</title>

    </head>

    <body>

        <h1>Task index from GET</h1>

        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

        <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">POST</button>

        </form>

    </body>

</html>